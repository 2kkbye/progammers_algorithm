#include <string>
#include <vector>
#include<iostream>

using namespace std;

string solution(int x, int y) {
    int initMonth = 1;
    int initDay = 1;
    int maxDay[] = { 0,31,29,31,30,31,30,31,31,30,31,30,31 };

    string day[] = { "SUN","MON","TUE","WED","THU","FRI","SAT" };
    int nowDay = 5;

    string answer = "";
    for (int a = 1; a < 1000; a++) {
        if (initMonth == x && initDay == y) {
            answer = day[nowDay];
            break;
        }
        initDay++;
        if (initDay > maxDay[initMonth]) {
            initDay = 1;
            initMonth++;
        }
        nowDay++;
        if (nowDay == 7) {
            nowDay = 0;
        }
    }

    return answer;
}
int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cout << solution(1, 2);
}