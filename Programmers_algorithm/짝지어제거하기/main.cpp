#include<iostream>
#include<stack>


using namespace std;

int solution(string s)
{
	stack<char> ch;
	for (int a = 0; a < s.length(); a++) {
		if (ch.size() == 0 || ch.top() !=s[a]) {
			ch.push(s[a]);
			continue;
		}
		ch.pop();
	}
	if (ch.size() == 0) return 1;
	return 0;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	cout << solution("baabaa");
}