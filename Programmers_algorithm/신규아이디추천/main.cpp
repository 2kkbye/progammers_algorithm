#include<iostream>
#include <string>
#include <vector>

using namespace std;

string solution(string new_id) {
    string answer = "";

    vector<char> v;
    //1단계
    for (int a = 0; a < new_id.length(); a++) {
        if (new_id[a] >= 'A' && new_id[a] <= 'Z') {
            new_id[a] = (char)(new_id[a] + 32);
        }
    }
    //2, 3, 4단계
    for (int a = 0; a < new_id.length(); a++) {
        if (v.size() == 15) break;
        if ((new_id[a] >= 'a' && new_id[a] <= 'z') || (new_id[a] >= '0' && new_id[a] <= '9') || new_id[a] == '.' || new_id[a] == '-' || new_id[a] == '_') {
            if (new_id[a] == '.' && v.size() == 0) continue;
            if (new_id[a] == '.' && v[v.size() - 1] == '.') continue;
            v.push_back(new_id[a]);
        }
    }
    //5단계
    if (v.size()==0) {
        v.push_back('a');
    }
    if (v[v.size() - 1] == '.') {
        v.erase(v.end() - 1, v.end());
    }
    //7단계
    if (v.size() <= 2) {
        char temp = v[v.size() - 1];
        int tempSize = v.size();
        for (int a = 0; a < 3 - tempSize; a++) {
            v.push_back(temp);
        }
    }
    for (int a = 0; a < v.size(); a++) {
        if (a == v.size() - 1 && v[a] == '.') continue;
        answer = answer + v[a];
    }
    return answer;
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

    string new_id = "z-+.^.";
    cout<< solution(new_id);
}