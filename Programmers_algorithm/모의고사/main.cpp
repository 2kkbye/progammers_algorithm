#include<iostream>
#include<vector>

using namespace std;
vector<int> aInput;
vector<int> bInput;
vector<int> cInput;
int temp[] = { 3,1,2,4,5 };

void inputAnswer(int answerSize) {
	int indexA = 1;
	int indexB = 1;
	int indexC = 0;
	int tempIndexC = 0;
	for (int a = 0; a < answerSize; a++) {
		
		aInput.push_back(indexA++);
		if (indexA == 6) indexA = 1;

		if (a % 2 == 0) {
			bInput.push_back(2);
		}
		else {
			bInput.push_back(indexB++);
			if (indexB == 6) indexB = 1;
			if (indexB == 2) indexB++;
		}

		for (int b = 0; b < 1; b++) {
			cInput.push_back(temp[indexC]);
		}
		tempIndexC++;
		if (tempIndexC == 2) {
			indexC++;
			if (indexC == 5) indexC = 0;
			tempIndexC = 0;
		}
	}

	for (int a = 0; a < answerSize; a++) {
		cout << aInput[a];
	}
	cout << endl;
	for (int a = 0; a < answerSize; a++) {
		cout << bInput[a];
	}
	cout << endl;
	for (int a = 0; a < answerSize; a++) {
		cout << cInput[a];
	}
	cout << endl;
}

vector<int> solution(vector<int> answers) {
	inputAnswer(answers.size());
	int answerNum[] = { 0,0,0,0 };
	for (int a = 0; a < answers.size(); a++) {
		if (aInput[a] == answers[a]) answerNum[1]++;
		if (bInput[a] == answers[a]) answerNum[2]++;
		if (cInput[a] == answers[a]) answerNum[3]++;
	}
	int max = -1;
	vector<int> answer;
	for (int a = 1; a < 4; a++) {
		if (max > answerNum[a]) continue;
		if (max == answerNum[a]) { answer.push_back(a); continue; }
		if (max < answerNum[a]) {
			max = answerNum[a];
			answer.clear();
			answer.push_back(a);
		}
	}
	return answer;
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	//inputAnswer(100);

	vector<int> answers = { 1,2,3,4,5,1,2,3,4,5,2,2,2,2,2 };

	vector<int> answer = solution(answers);
	for (int a = 0; a < answer.size(); a++) {
		cout << answer[a];
	}
}