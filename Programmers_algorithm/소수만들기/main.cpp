#include<iostream>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#define	PRIME_SIZE 3000

using namespace std;
bool primeNum[PRIME_SIZE];

void init() {
	for (int a = 0; a < PRIME_SIZE; a++) {
		primeNum[a] = true;
	}
}
void primeCheckNum() {
	for (int a = 2; a < PRIME_SIZE; a++) {
		if (!primeNum[a]) continue;
		int index = 2;
		while (index * a < PRIME_SIZE) {
			primeNum[index * a] = false;
			index++;
		}
	}
}
int solution(int nums[], size_t nums_len) {
	init();
	primeCheckNum();
	int answer = 0;
	int sum = 0;
	for (int a = 0; a < nums_len - 2; a++) {
		sum += nums[a];
		for (int b = a+1; b < nums_len - 1; b++) {
			sum += nums[b];
			for (int c = b+1; c < nums_len; c++) {
				sum += nums[c];
				if (primeNum[sum]) {
					answer++;
				}
				sum -= nums[c];
			}
			sum -= nums[b];
		}
		sum -= nums[a];
	}
	return answer;
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int nums[] = { 1,2,3,4 };
	//solution(nums, size_t num_len);
	cout<< solution(nums, 4);

}