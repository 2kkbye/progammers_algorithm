#include <string>
#include <vector>
#include <iostream>

#define SIZE 16

using namespace std;

int map1[SIZE][SIZE];
int map2[SIZE][SIZE];
void init() {
    for (int a = 0; a < SIZE; a++) {
        for (int b = 0; b < SIZE; b++) {
            map1[a][b] = 0;
            map2[a][b] = 0;
        }
    }
}
void insert(int num, int index, int size, int temp) {
    vector<int> dNum;
    while (num > 1) {
        dNum.push_back(num % 2);
        num /= 2;
    }
    dNum.push_back(num);
    for (int a = size - dNum.size(); a >= 1; a--) {
        dNum.push_back(0);
    }
    if (temp == 0) {
        for (int a = dNum.size() - 1; a >= 0; a--) {
            map1[index][size - 1 - a] = dNum[a];
        }
    }
    else {
        for (int a = dNum.size() - 1; a >= 0; a--) {
            map2[index][size - 1 - a] = dNum[a];
        }
    }


}

vector<string> solution(int n, vector<int> arr1, vector<int> arr2) {
    init();
    for (int a = 0; a < arr1.size(); a++) {
        insert(arr1[a], a, n, 0);
        insert(arr2[a], a, n, 1);
    }
    vector<string> answer;

    for (int a = 0; a < n; a++) {
        string tempAnswer = "";
        for (int b = 0; b < n; b++) {
            if (map1[a][b] == 0 && (map1[a][b] == map2[a][b])) {
                tempAnswer += ' ';
            }
            else {
                tempAnswer += '#';
            }
        }
        answer.push_back(tempAnswer);
    }
    return answer;
}
int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

}