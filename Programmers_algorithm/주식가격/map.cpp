#include <string>
#include <vector>
#include<iostream>
using namespace std;

vector<int> solution(vector<int> prices) {
    vector<int> answer;
    for (int a = 0; a < prices.size() - 1; a++) {
        int temp = 0;
        for (int b = a + 1; b < prices.size(); b++) {
            temp++;
            if (prices[a] > prices[b]) break;
        }
        answer.push_back(temp);
    }
    answer.push_back(0);
    return answer;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

    vector<int> v = { 0,1,2,3,4 };
    cout << solution(v).size();
}