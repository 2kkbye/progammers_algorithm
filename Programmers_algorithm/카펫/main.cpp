#include <string>
#include <vector>
#include<iostream>

using namespace std;

vector<int> solution(int brown, int yellow) {
    int sum = brown + yellow;
    vector<int> temp;
    vector<int> answer;
    for (int a = 1; a <= sum; a++) {
        if (sum % a != 0) continue;
        temp.push_back(a);
    }
    int right;
    int left;
    if (temp.size() % 2 != 0) {
        right = temp.size() / 2;
        left = temp.size() / 2;
        while (true) {
            if (((temp[right] - 2) * (temp[left] - 2)) == yellow) break;
            right++; left--;
        }
    }
    else {
        right = temp.size() / 2;
        left = (temp.size() / 2) - 1;
        while (true) {
            if (((temp[right] - 2) * (temp[left] - 2)) == yellow) break;
            right++; left--;
        }
    }
    answer.push_back(temp[right]);
    answer.push_back(temp[left]);
    return answer;
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cout << solution(8, 1).size();
}