#include <string>
#include <vector>
#include <iostream>
#define SIZE 31

int clothNum[SIZE];

using namespace std;

void init() {
    for (int a = 1; a < SIZE; a++) {
        clothNum[a] = 0;
    }
}

int solution(int n, vector<int> lost, vector<int> reserve) {
    init();
    int answer = 0;
    for (int a = 0; a < reserve.size(); a++) {
        clothNum[reserve[a]] = 2;
    }
    for (int a = 0; a < lost.size(); a++) {
        clothNum[lost[a]]--;
    }
    for (int a = 1; a <= n; a++) {
        if (clothNum[a] != 2) continue;
        if (a - 1 >= 0 && clothNum[a - 1] == -1) {
            clothNum[a - 1]++;
            clothNum[a]--;
            continue;
        }
        else if (a + 1 <= n && clothNum[a + 1] == -1) {
            clothNum[a + 1]++;
            clothNum[a]--;
            continue;
        }
    }
    for (int a = 1; a <= n; a++) {
        if (clothNum[a] == -1) continue;
        answer++;
    }
    return answer;
}
int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    vector<int> a;
    vector<int> b;
    a.push_back(1);
    b.push_back(2);
    cout << solution(2, a, b);
}