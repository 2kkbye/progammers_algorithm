#include <string>
#include <vector>
#include<iostream>
using namespace std;

vector<vector<int>> solution(vector<vector<int>> arr1, vector<vector<int>> arr2) {
    vector<vector<int>> answer;

    for (int a = 0; a < arr1.size(); a++) {
        vector<int> temp;
        for (int b = 0; b < arr2[0].size(); b++) {
            int sum = 0;
            for (int c = 0; c < arr2.size(); c++) {
                sum += arr1[a][c] * arr2[c][b];
            }
            temp.push_back(sum);
        }
        answer.push_back(temp);
    }
    return answer;
}
int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    vector<vector<int>> arr1 = { {1,4},{3,2},{4,1} };
    vector<vector<int>> arr2 = { {3,3},{3,3}};
    cout << solution(arr1, arr2).size();
}