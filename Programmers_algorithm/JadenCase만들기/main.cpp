#include <string>
#include <vector>
#include <iostream>
using namespace std;
//소문자가 대문자보다 32큼
char checkB(char c) {
    if (c >= 'a' && c <= 'z') {
        return (int)c - 32;
    }
    else {
        return c;
    }
}
char checkS(char c) {
    if (c >= 'A' && c <= 'Z') {
        return (int)c + 32;
    }
    else {
        return c;
    }

}
string solution(string s) {
    s[0] = checkB(s[0]);
    for (int a = 1; a < s.length(); a++) {
        if (s[a] == ' ') {
            if (s[a + 1] == ' ') {
                continue;
            }
            s[a + 1] = checkB(s[a + 1]);
            a += 1;
        }
        else {
            s[a] = checkS(s[a]);
        }
    }
    return s;
}
int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cout << solution("for the Last game");
}