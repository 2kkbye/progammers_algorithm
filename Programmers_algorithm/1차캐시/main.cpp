#include <string>
#include <vector>
#include<iostream>
#define SIZE 31

using namespace std;

string cache[SIZE];
int cacheIndex[SIZE];
int nowCacheSize;
string changeStr(string str) {
    string ans = str;
    for (int a = 0; a < str.length(); a++) {
        if (ans[a] >= 'a' && ans[a] <= 'z') continue;
        char c = (char)((int)str[a] + 32);
        ans[a] = c;
    }
    return ans;
}
void init() {
    for (int a = 0; a < SIZE; a++) {
        cache[a] = "";
        cacheIndex[a] = 0;
    }
}
bool checkCache(int cacheSize, string city, int nowIndex) {
    for (int a = 0; a < cacheSize; a++) {
        if (cache[a] != city) continue;
        cacheIndex[a] = nowIndex;
        return true;
    }
    return false;
}
int checkIndex(int cacheSize) {
    int min = 1000000000;
    int minIndex = -1;
    for (int a = 0; a < cacheSize; a++) {
        if (min > cacheIndex[a]) {
            min = cacheIndex[a];
            minIndex = a;
        }
    }
    return minIndex;
}
int solution(int cacheSize, vector<string> cities) {
    if (cacheSize == 0) {
        return cities.size() * 5;
    }
    nowCacheSize = 0;
    init();
    int answer = 0;
    for (int a = 0; a < cities.size(); a++) {
        string temp = changeStr(cities[a]);
        cities[a] = temp;
        if (checkCache(cacheSize, cities[a], a)) {
            answer += 1;
            continue;
        }
        if (nowCacheSize < cacheSize) {
            answer += 5;
            cache[nowCacheSize] = cities[a];
            cacheIndex[nowCacheSize] = a;
            nowCacheSize++;
            continue;
        }
        answer += 5;
        int min = checkIndex(cacheSize);
        cache[min] = cities[a];
        cacheIndex[min] = a;
    }
    return answer;
}
int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    vector<string> v = { "newyork", "seoul" };
    cout << solution(2, v);
}