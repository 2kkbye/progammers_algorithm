#include<iostream>
#include<vector>
#define SIZE 200001

using namespace std;

int arr[SIZE];
void init() {
    for (int a = 0; a < SIZE; a++) {
        arr[a] = 0;
    }
}
int solution(vector<int> nums)
{
    init();
    int answer = 0;
    for (int a = 0; a < nums.size(); a++) {
        if (arr[nums[a]] == 0) {
            answer++;
        }
        arr[nums[a]]++;
    }
    if (answer > (nums.size() / 2)) {
        answer = nums.size() / 2;
    }
    return answer;
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
    vector<int> nums = { 1,1,1,1,1,2,2,3 };
    cout<<solution(nums);
}