#include <string>
#include <vector>
#include<iostream>
#include<map>
using namespace std;

vector<string> solution(vector<string> record) {
    map<string, string> tv;
    vector<string> id;
    vector<char> ins;

    for (int a = 0; a < record.size(); a++) {
        record[a] += ' ';
        char tc = record[a][0];
        int ti = 0;
        if (tc == 'L' || tc == 'E') {
            ti = 6;
            ins.push_back(tc);
        }
        else {
            ti = 7;
        }
        string temp = "";
        vector<string> id_name;
        for (int b = ti; b < record[a].size(); b++) {
            if (record[a][b] == ' ') {
                id_name.push_back(temp);
                temp = "";
                continue;
            }
            temp += record[a][b];
        }
        //id_name[0] = id, id_name[1] = name
        if (tc == 'L') {
            id.push_back(id_name[0]);
        }
        else if (tc == 'E') {
            id.push_back(id_name[0]);
            //있을 경우
            if (tv.find(id_name[0]) != tv.end()) {
                //있는데 name이 다른 경우
                if (tv[id_name[0]] != id_name[1]) {
                    tv[id_name[0]] = id_name[1];
                }
            }
            //없는 경우
            else {
                tv.insert({ id_name[0],id_name[1] });
            }
        }
        else if (tc == 'C') {
            tv[id_name[0]] = id_name[1];
        }
    }
    vector<string> answer;
    for (int a = 0; a < ins.size(); a++) {
        string sr = "";
        if (ins[a] == 'L') {
            sr = tv[id[a]] + "님이 나갔습니다.";
        }
        else {
            sr = tv[id[a]] + "님이 들어왔습니다.";
        }
        answer.push_back(sr);
    }


    return answer;
}
int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    vector<string> v = { {"Enter uid1234 Muzi"}, {"Enter uid4567 Prodo"},{"Leave uid1234"},{"Enter uid1234 Prodo"},{"Change uid4567 Ryan"} };
    cout << solution(v)[0];
}