#include<iostream>
#include<queue>

using namespace std;

bool solution(string s)
{
    queue<char> q1;

    for (int a = 0; a < s.length(); a++) {
        if (s[a] == '(') {
            q1.push(s[a]);
        }
        else {
            if (q1.size() == 0) return false;
            q1.pop();
        }
    }
    if (q1.size() == 0) return true;
    return false;
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

    cout << solution("((()))");
}