#include<iostream>
#include <vector>
#include <algorithm>

using namespace std;

int solution(vector<int> d, int budget) {
    int answer = d.size();
    sort(d.begin(), d.end());
    int sum = 0;
    for (int a = 0; a < d.size(); a++) {
        sum += d[a];
        if (sum > budget) {
            answer = a;
            break;
        }
    }
    return answer;
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

}