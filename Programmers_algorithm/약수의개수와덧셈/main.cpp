#include<iostream>

#include <string>
#include <vector>
#define SIZE 1001

bool visit[SIZE];
void init(int num) {
    for (int a = 2; a < num; a++) {
        visit[a] = false;
    }
}
using namespace std;
int checkNum(int num) {
    if (num == 1) return 1;
    int answer = 2;
    for (int a = 2; a < num; a++) {
        if (num % a == 0 && !visit[a]) {
            visit[a] = true;
            if (num / a == a) {
                answer += 1;
                continue;
            }
            visit[num / a] = true;
            answer += 2;
        }
    }
    init(num);
    return answer;
}
int solution(int left, int right) {
    init(SIZE);
    int answer = 0;
    for (int a = left; a <= right; a++) {
        if (checkNum(a) % 2 == 0) answer += a;
        else answer -= a;
    }
    return answer;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

    cout << solution(13, 17);
}