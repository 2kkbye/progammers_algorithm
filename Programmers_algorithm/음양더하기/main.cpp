#include<iostream>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

using namespace std;

int solution(int absolutes[], size_t absolutes_len, bool signs[], size_t signs_len) {
	int answer = 0;
	for (int a = 0; a < absolutes_len; a++) {
		if (!signs[a]) {
			answer += (absolutes[a] * -1);
			continue;
		}
		answer += absolutes[a];
	}
	return answer;
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int absolutes[] = { 1,2,3,4,5,6,7,8,9,10 };
	bool signs[] = { true,true, true, true, true, true, true, true, true, true};
	cout << solution(absolutes, 10, signs, 10);
}