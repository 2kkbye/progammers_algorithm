#include <string>
#include <vector>
#include<iostream>

using namespace std;

int checkOne(int n) {
    int answer = 0;
    while (n > 1) {
        if (n % 2 == 1) {
            answer++;
        }
        n /= 2;
    }
    if (n == 1) answer++;
    return answer;
}

int solution(int n) {
    int tempN = checkOne(n);
    int answer = 0;
    for (int a = n + 1; a <= 2000000; a++) {
        int temp = checkOne(a);
        if (temp == tempN) {
            answer = a;
            break;
        }
    }
    return answer;
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cout << solution(10);

    return 0;
}