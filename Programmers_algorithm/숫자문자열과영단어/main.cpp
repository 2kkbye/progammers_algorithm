#include <string>
#include <vector>
#include<iostream>
using namespace std;

string number[] = { "zero","one","two","three","four","five","six","seven","eight","nine" };
int findNum(char c1, char c2) {
    for (int a = 0; a < 10; a++) {
        if (c1 == number[a][0] && c2 == number[a][1]) {
            return a;
        }
    }
}
int solution(string s) {
    vector<int> temp;
    for (int a = 0; a < s.length(); a++) {
        if (s[a] >= '0' && s[a] <= '9') {
            temp.push_back((int)s[a] - 48);
            continue;
        }
        else {
            int findIndex = findNum(s[a], s[a + 1]);
            temp.push_back(findIndex);
            a += number[findIndex].length() - 1;
        }
    }
    int answer = 0;
    int nt = 1;
    for (int a = temp.size() - 1; a >= 0; a--) {
        answer += (nt * temp[a]);
        nt *= 10;
    }


    return answer;
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cout << solution("1one");
}