#include <string>
#include<iostream>
#include<vector>
#include<algorithm>
#define SIZE 1000

using namespace std;
bool visit[SIZE];
void init() {
    for (int a = 0; a < SIZE; a++) {
        visit[a] = false;
    }
}
int solution(string str1, string str2) {
    init();
    //대문자가 32작음
    vector<string> st1;
    vector<string> st2;

    string temp = "";
    for (int a = 0; a < str1.length(); a++) {
        char c = str1[a];
        if (c >= 'A' && c <= 'Z') {
            c = char(c + 32);
        }
        temp += c;
        if (temp.length() == 2) {
            if ((temp[0] >= 'a' && temp[0] <= 'z') && (temp[1] >= 'a' && temp[1] <= 'z')) {
                st1.push_back(temp);
            }
            temp = "";
            temp += c;
        }
    }
    temp = "";
    for (int a = 0; a < str2.length(); a++) {
        char c = str2[a];
        if (c >= 'A' && c <= 'Z') {
            c = char(c + 32);
        }
        temp += c;
        if (temp.length() == 2) {
            if ((temp[0] >= 'a' && temp[0] <= 'z') && (temp[1] >= 'a' && temp[1] <= 'z')) {
                st2.push_back(temp);
            }
            temp = "";
            temp += c;
        }
    }
    sort(st1.begin(), st1.end());
    sort(st2.begin(), st2.end());

    int index = 0;

    //교집합
    vector<string> t1;
    //합집합
    vector<string> t2;
    for (int a = 0; a < st1.size(); a++) {
        bool check = false;
        for (int b = 0; b < st2.size(); b++) {
            if (st1[a] == st2[b] && !visit[b]) {
                visit[b] = true;
                check = true;
                t1.push_back(st1[a]);
                t2.push_back(st1[a]);
                break;
            }
        }
        if (!check) {
            t2.push_back(st1[a]);
        }
    }
    for (int a = 0; a < st2.size(); a++) {
        if (visit[a]) continue;
        t2.push_back(st2[a]);
    }
    //for(int a = 0;a<t1.size();a++){
        //cout<<t1[a]<<endl;
    //}
    //cout<<"--------"<<endl;
    //for(int a = 0;a<t2.size();a++){
        //cout<<t2[a]<<endl;
    //}
    float ans;
    if (t2.size() == 0) ans = 1.0;
    else {
        ans = (float(t1.size()) / float(t2.size()));
    }
    return (int)(ans * 65536);
}
int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cout << solution("FRANCE", "french");
    return 0;
}