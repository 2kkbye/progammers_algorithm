#include <string>
#include <vector>
#include <algorithm>
#include<iostream>
#define SIZE 502

using namespace std;

int playNum[SIZE];
double fail[SIZE];
void init() {
    for (int a = 1; a < SIZE; a++) {
        playNum[a] = 0;
    }
}

vector<int> solution(int N, vector<int> stages) {
    init();
    vector<int> answer;
    vector<double> failNum;

    for (int a = 0; a < stages.size(); a++) {
        playNum[stages[a]]++;
    }
    int size = stages.size();
    double temp;

    for (int a = 1; a <= N; a++) {
        temp = 0;
        if (playNum[a] != 0 || size != 0) {
            temp = (double)playNum[a] / (double)size;
        }
        fail[a] = temp;
        bool type = true;
        for (int b = 0; b < failNum.size(); b++) {
            if (failNum[b] == temp) {
                type = false;
                break;
            }
        }
        if (type) {
            failNum.push_back(temp);
        }
        size -= playNum[a];
    }
    //실패율은 내림차순, 번호는 작은것부터 -> 마지막에 있는 수는 실패율 가장 큰 것.
    sort(failNum.begin(), failNum.end());
    for (int a = failNum.size() - 1; a >= 0; a--) {
        temp = failNum[a];
        for (int b = 1; b <= N; b++) {
            if (fail[b] == temp) {
                answer.push_back(b);
            }
        }
    }
    return answer;
}
int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);
    
    vector<int> temp;
    temp.push_back(0);
    cout << solution(1, temp).size();
}