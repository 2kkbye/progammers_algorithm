#include <string>
#include <vector>
#include<iostream>
#define SIZE 100000

using namespace std;
long long arr[SIZE];
void init() {
    for (int a = 0; a < SIZE; a++) {
        arr[a] = 0;
    }
}

int solution(int n) {
    int answer = 0;
    arr[1] = 1;
    for (int a = 2; a <= n; a++) {
        arr[a] = (arr[a - 1] + arr[a - 2]) % 1234567;
    }
    return arr[n];
}
int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cout << solution(100);
}
