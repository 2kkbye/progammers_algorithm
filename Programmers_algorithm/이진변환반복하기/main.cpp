#include <string>
#include <vector>
#include<iostream>
using namespace std;

vector<int> solution(string s) {
    int cnt = 0;
    int zerocnt = 0;
    while (s != "1") {
        string temp = "";
        cnt++;

        for (int a = 0; a < s.length(); a++) {
            if (s[a] == '0') {
                zerocnt++;
                continue;
            }
            temp += s[a];
        }

        int num = temp.size();
        s = "";
        while (num > 1) {
            s = s + ((char)((num % 2) + 48));
            num /= 2;
        }
        s = s + ((char)((num % 2) + 48));
    }
    vector<int> answer;
    answer.push_back(cnt);
    answer.push_back(zerocnt);

    return answer;
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cout << solution("11111000").size();
}