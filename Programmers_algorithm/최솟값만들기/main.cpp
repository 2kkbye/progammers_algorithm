#include <iostream>
#include<vector>
#include<algorithm>

using namespace std;

int solution(vector<int> A, vector<int> B)
{
    sort(A.begin(), A.end());
    sort(B.begin(), B.end());
    int answer = 0;
    for (int a = 0; a < A.size(); a++) {
        int su = A[a] * B[A.size() - a - 1];
        answer += su;
    }
    return answer;
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    vector<int> A;
    vector<int> B;
    for (int a = 1; a < 10; a++) {
        A.push_back(a);
        B.push_back(a);
    }
    //cout << "start"<<endl;
    solution(A, B);
}