#include<Iostream>
#include<vector>

using namespace std;

int solution(vector<int> a, vector<int> b) {
    int temp = 0;
    int answer = 0;
    for (int t = 0; t < a.size(); t++) {
        temp = a[t] * b[t];
        answer += temp;
    }
    return answer;
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);


}