#include<iostream>
#include<vector>

using namespace std;

int solution(vector<vector<int> > temp)
{
    int answer = -1;
    vector<vector<int>> land = temp;
    //1번 row 부터 row -1과 더해가면서 비교해가야한다.
    for (int row = 1; row < land.size(); row++) {
        //진행하고자 하는 열
        for (int col = 0; col < 4; col++) {
            int max = -1;
            //전 행의 열들을 비교해서 자기자신과 더해서 최댓값을 구해야한다.
            for (int pre = 0; pre < 4; pre++) {
                if (pre == col) continue;
                if ((temp[row][col] + temp[row - 1][pre]) > max) {
                    max = temp[row][col] + temp[row - 1][pre];
                }
            }
            temp[row][col] = max;
        }
    }
    for (int a = 0; a < 4; a++) {
        if (answer < temp[temp.size() - 1][a]) {
            answer = temp[temp.size() - 1][a];
        }
    }
    return answer;
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	vector<vector<int>> temp = { {1,2,3,5},{5,6,7,8},{4,3,2,1} };
	cout << solution(temp);

}