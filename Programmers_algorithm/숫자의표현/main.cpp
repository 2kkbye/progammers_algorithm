#include <string>
#include <vector>
#include<Iostream>

using namespace std;

int solution(int n) {
    int answer = 1;
    for (int a = (n / 2 + 1); a >= 1; a--) {
        int sum = 0;
        int temp = a;
        while (true) {
            sum += temp;
            if (sum == n) {
                answer++;
                break;
            }
            if (sum > n) break;
            temp--;
        }
    }
    return answer;
}
int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cout << solution(10);
}