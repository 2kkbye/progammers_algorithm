#include<iostream>
#include <vector>
#define BAG_SIZE 901

using namespace std;

int BAG_INDEX = -1;
int BAG[BAG_SIZE];
void init() {
    for (int a = 0; a < BAG_SIZE; a++) {
        BAG[a] = 0;
    }
}
void insertBag(int num) {
    BAG_INDEX++;
    BAG[BAG_INDEX] = num;
}
bool checkBag(int num) {
    if (BAG_INDEX == -1) return true;
    if (BAG[BAG_INDEX] == num) return false;
    return true;
}
void deleBag() {
    BAG[BAG_INDEX] = 0;
    BAG_INDEX--;
}

int solution(vector<vector<int>> board, vector<int> moves) {
    int answer = 0;
    for (int a = 0; a < moves.size(); a++) {
        int temp = moves[a]-1;
        int num;
        for (int b = 0; b < board.size(); b++) {
            if (board[b][temp] == 0) continue;
            num = board[b][temp];
            board[b][temp] = 0;
            if (checkBag(num)) {
                insertBag(num);
            }
            else {
                deleBag();
                answer += 2;
            }
            break;
        }
    }

    return answer;
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);
    vector<vector<int>> board = { {0,0,0,0,0} ,{0,0,1,0,3},{0,2,5,0,1},{4,2,4,4,2},{3,5,1,3,1} };
    vector<int> moves = { 1,5,3,5,1,2,1,4 };
    solution(board, moves);
}