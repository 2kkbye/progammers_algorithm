#include<iostream>
#include <vector>

using namespace std;
int countZero(vector<int> lottos) {
    int ans = 0;
    for (int a = 0; a < lottos.size(); a++) {
        if (lottos[a] != 0) continue;
        ans++;
    }
    return ans;
}
int countAns(vector<int> lottos, vector<int> win_nums) {
    int ans = 0;
    for (int a = 0; a < lottos.size(); a++) {
        for (int b = 0; b < win_nums.size(); b++) {
            if (lottos[a] != win_nums[b]) continue;
            ans++;
            break;
        }
    }
    return ans;
}
vector<int> solution(vector<int> lottos, vector<int> win_nums) {
    vector<int> answer;
    int scoreAns[] = { 6,6,5,4,3,2,1 };
    int maxAns = 0, minAns = 0, zeroCount = 0;
    zeroCount = countZero(lottos);
    minAns = countAns(lottos, win_nums);
    maxAns = minAns + zeroCount;

    answer.push_back(scoreAns[maxAns]);
    answer.push_back(scoreAns[minAns]);
    return answer;
}
int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    int a = 1000000000;
    cout << a;

}