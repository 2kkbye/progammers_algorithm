#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

string solution(string s) {
    s = s + ' ';
    vector<long> num;
    bool type = true;
    long tNum = 0;
    for (int a = 0; a < s.length(); a++) {
        if (s[a] == ' ') {
            if (!type) {
                tNum *= -1;
            }
            type = true;
            num.push_back(tNum);
            tNum = 0;
            continue;
        }
        if (s[a] == '-') {
            type = false;
            continue;
        }
        if (tNum == 0) {
            tNum = (int)s[a] - 48;
        }
        else {
            tNum *= 10;
            tNum += ((int)s[a] - 48);
        }
    }

    sort(num.begin(), num.end());
    string answer = to_string(num[0]);
    string answer2 = to_string(num[num.size() - 1]);
    answer = answer + ' ' + answer2;
    return answer;
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cout << solution("1 2 3");
}