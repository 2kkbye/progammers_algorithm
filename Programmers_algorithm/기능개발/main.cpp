#include <string>
#include <vector>
#include<iostream>
using namespace std;

vector<int> solution(vector<int> progresses, vector<int> speeds) {
    vector<int> answer;
    int index = 0;
    for (int a = 1; a <= 100; a++) {
        if (index == progresses.size()) break;
        for (int b = index; b < progresses.size(); b++) {
            progresses[b] = progresses[b] + speeds[b];
        }
        if (progresses[index] >= 100) {
            int temp = 1;
            for (int b = index + 1; b < progresses.size(); b++) {
                if (progresses[b] >= 100) temp++;
                else break;
            }
            answer.push_back(temp);
            index += temp;
        }
    }
    return answer;
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    vector<int> progresses;
    vector<int> speeds;

    progresses.push_back(55);
    progresses.push_back(65);
    progresses.push_back(70);
    speeds.push_back(15);
    speeds.push_back(30);
    speeds.push_back(45);
    cout<< solution(progresses, speeds).size();
}