#include<iostream>
#include<vector>
#include<unordered_map>

using namespace std;

string solution(vector<string> participant, vector<string> completion) {
	string answer = "";
	unordered_map<string, int> map;
	for (int a = 0; a < participant.size(); a++) {
		map[participant[a]]++;
	}

	for (int a = 0; a < completion.size(); a++) {
		map[completion[a]]--;
	}

	for (auto elem : map) {
		if (elem.second == 0) continue;
		answer = elem.first;
	}
	return answer;
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	vector<string> participant = { "marina", "josipa", "nikola", "vinko", "filipa" };
	vector<string> completion = { "josipa", "filipa", "marina", "nikola" };

	cout<<solution(participant, completion);
}