#include <string>
#include<iostream>
#include<vector>
#define SIZE 11

using namespace std;

int dx[] = { -1,0,1,0 };
int dy[] = { 0,1,0,-1 };
vector<vector<int>> sou;
vector<vector<int>> des;

bool check(int x, int y, int rx, int ry) {
    for (int a = 0; a < sou.size(); a++) {
        if (sou[a][0] == x && sou[a][1] == y) {
            if (des[a][0] == rx && des[a][1] == ry) {
                return false;
            }
        }
    }
    for (int a = 0; a < sou.size(); a++) {
        if (des[a][0] == x && des[a][1] == y) {
            if (sou[a][0] == rx && sou[a][1] == ry) {
                return false;
            }
        }
    }
    return true;
}
int solution(string dirs) {
    int index = 0;
    int x = 5, y = 5;
    for (int a = 0; a < dirs.length(); a++) {
        if (dirs[a] == 'U') {
            index = 0;
        }
        else if (dirs[a] == 'R') {
            index = 1;
        }
        else if (dirs[a] == 'D') {
            index = 2;
        }
        else if (dirs[a] == 'L') {
            index = 3;
        }
        int rx = x + dx[index];
        int ry = y + dy[index];
        if (rx < 0 || ry < 0 || rx>10 || ry >10) continue;
        if (check(x, y, rx, ry)) {
            vector<int> s;
            vector<int> d;
            s.push_back(x);
            s.push_back(y);
            d.push_back(rx);
            d.push_back(ry);
            sou.push_back(s);
            des.push_back(d);
        }
        x = rx;
        y = ry;
    }
    int answer = sou.size();
    return answer;
}
int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cout<<solution("ULURRDLLU");
}