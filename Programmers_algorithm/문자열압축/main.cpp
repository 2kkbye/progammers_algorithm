#include <string>
#include <vector>
#include<iostream>

using namespace std;

int solution(string s) {
    int answer = s.length();
    s += " ";
    // size = 검사하고자 하는 대상의 개수
    for (int size = 1; size <= s.length()/2; size++) {
        string init="";

        //검사하는 초기 문자열 세팅
        for (int a = 0; a < size; a++) {
            init += s[a];
        }
        int count = 1;
        vector<char> te;
        for (int a = 0 + size; a < s.length(); a = a + size) {
            bool type = true;
            for (int b = 0; b < init.length(); b++) {
                if (init[b] == s[a + b]) continue;
                type = false;
                break;
            }
            if (type) {
                count++;
                continue;
            }
            else {
                if (count != 1) {
                    vector<int> tNum;

                    while (count > 9) {
                        tNum.push_back(count % 10);
                        count /= 10;
                    }
                    tNum.push_back(count);
                    for (int a = tNum.size() - 1; a >= 0; a--) {
                        te.push_back((char)tNum[a] + 48);
                    }
                }
                for (int b = 0; b < init.length(); b++) {
                    te.push_back(init[b]);
                }
                count = 1;
                for (int b = 0; b < init.length(); b++) {
                    if (s[a + b] == ' ' || a + b >= s.length()) {
                        for (int c = 0; c < b; c++) {
                            te.push_back(init[c]);
                        }
                        break;
                    }
                    init[b] = s[a + b];
                }
            }
        }
        if (answer > te.size() && te.size()!=0) {
            answer = te.size();
        }

    }
    
    return answer;
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    //cout << solution("xababcdcdababcdcd");
    cout << solution("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

}