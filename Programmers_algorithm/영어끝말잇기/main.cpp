#include <string>
#include <vector>
#include <iostream>
#include<map>

using namespace std;

vector<int> solution(int n, vector<string> words) {
    vector<int> answer;
    map<string, int> tv;
    int person = 2;
    int period = 1;
    tv.insert({ words[0] , 1 });
    bool check = true;
    for (int a = 1; a < words.size(); a++) {
        if (words[a - 1][words[a - 1].length() - 1] != words[a][0]) {
            check = false;
            break;
        }
        if (tv.find(words[a]) != tv.end()) {
            check = false;
            break;
        }
        else {
            person++;
            if (person > n) {
                person = 1;
                period++;
            }
            tv.insert({ words[a] , person });
        }
    }
    if (!check) {
        answer.push_back(person);
        answer.push_back(period);
    }
    else {
        answer.push_back(0);
        answer.push_back(0);
    }

    return answer;
}
int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);
    vector<string> v = { "tank", "kick", "know", "wheel", "land", "dream", "mother", "robot", "tank" };
    cout << solution(3,v).size();
}