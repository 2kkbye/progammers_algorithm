#include <string>
#include <vector>
#include<iostream>
#define SIZE 31

using namespace std;

char map[SIZE][SIZE];
bool mapCheck[SIZE][SIZE];

void init() {
    for (int a = 0; a < SIZE; a++) {
        for (int b = 0; b < SIZE; b++) {
            map[a][b] = ' ';
            mapCheck[a][b] = false;
        }
    }
}
void initCheck(int m, int n) {
    for (int a = 0; a < m; a++) {
        for (int b = 0; b < n; b++) {
            mapCheck[a][b] = false;
        }
    }
}
int checkAns(int m, int n) {
    int ans = 0;
    for (int a = 0; a < m; a++) {
        for (int b = 0; b < n; b++) {
            if (mapCheck[a][b]) ans++;
        }
    }
    return ans;
}
void checkMap(int m, int n) {
    for (int a = 0; a < m - 1; a++) {
        for (int b = 0; b < n - 1; b++) {
            if (map[a][b] == ' ') continue;
            if (map[a][b] == map[a][b + 1] && map[a][b] == map[a + 1][b] && map[a][b] == map[a + 1][b + 1]) {
                mapCheck[a][b] = true;
                mapCheck[a + 1][b] = true;
                mapCheck[a][b + 1] = true;
                mapCheck[a + 1][b + 1] = true;
            }
        }
    }
}
void eraseMap(int m, int n) {
    for (int a = 0; a < m; a++) {
        for (int b = 0; b < n; b++) {
            if (mapCheck[a][b]) map[a][b] = ' ';
        }
    }
}
void inputChar(vector<string> board) {
    for (int a = 0; a < board.size(); a++) {
        for (int b = 0; b < board[a].length(); b++) {
            map[a][b] = board[a][b];
        }
    }
}
void print(int m, int n) {
    for (int a = 0; a < m; a++) {
        for (int b = 0; b < n; b++) {
            cout << map[a][b] << " ";
        }
        cout << endl;
    }
    cout << "=================================" << endl;
}
void moveBlock(int m, int n) {
    //��
    for (int b = 0; b < n; b++) {
        int zeroCnt = 0;
        for (int a = m - 1; a >= 0; a--) {
            if (map[a][b] != ' ' && zeroCnt == 0) continue;
            if (map[a][b] == ' ') {
                zeroCnt++;
                continue;
            }
            char temp = map[a][b];
            map[a][b] = ' ';
            map[a + zeroCnt][b] = temp;
            a = a + zeroCnt;
            zeroCnt = 0;
        }
    }
}
int solution(int m, int n, vector<string> board) {
    init();
    inputChar(board);
    int answer = 0;
    while (true) {
        initCheck(m, n);
        checkMap(m, n);
        eraseMap(m, n);
        int tans = checkAns(m, n);
        if (tans == 0) break;
        answer += tans;
        //print(m, n);
        moveBlock(m, n);
        //print(m, n);
    }

    return answer;
}
int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    vector<string> board = { "TTTANT", "RRFACC", "RRRFCC", "TRRRAA", "TTMMMF", "TMMTTJ" };
    cout<<solution(6, 6, board);
}