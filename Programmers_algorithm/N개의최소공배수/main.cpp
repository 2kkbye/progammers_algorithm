#include <string>
#include <vector>
#include <algorithm>
#include<iostream>

using namespace std;

int solution(vector<int> arr) {
    int answer = 0;
    sort(arr.begin(), arr.end());
    for (int a = arr[arr.size() - 1]; a <= 100000000; a++) {
        bool check = true;
        for (int b = 0; b < arr.size(); b++) {
            if ((a % arr[b]) != 0) {
                check = false;
                break;
            }
        }
        if (check) {
            answer = a;
            break;
        }
    }
    return answer;
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    vector<int> v;
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);
    v.push_back(4);
    cout << solution(v);

}