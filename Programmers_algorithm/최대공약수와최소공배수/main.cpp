#include <string>
#include <vector>
#include <iostream>
using namespace std;

vector<int> solution(int n, int m) {
    vector<int> answer;
    if (n > m) {
        int temp = m;
        m = n;
        n = temp;
    }
    int max = 1;
    int min = 1;
    for (int a = 2; a <= n; a++) {
        if ((n == 1) || (m == 1)) break;
        if (n % a != 0 || m % a != 0) continue;
        max *= a;
        n /= a;
        m /= a;
        a = 1;
    }
    answer.push_back(max);
    answer.push_back(max * n * m);
    return answer;
}
int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    for (int a = 0; a < solution(2, 4).size(); a++) {
        cout << solution(2, 4)[a];
        cout << " ";
    }
}