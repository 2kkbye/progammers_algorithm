#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

vector<int> solution(vector<int> array, vector<vector<int>> commands) {
	vector<int> answer;
	for (int a = 0; a < commands.size(); a++) {
		vector<int> temp;
		int i = commands[a][0]-1;
		int j = commands[a][1]-1;
		int k = commands[a][2]-1;
		for (int b = i; b <= j; b++) {
			temp.push_back(array[b]);
		}
		sort(temp.begin(), temp.end());
		answer.push_back(temp[k]);
	}
	return answer;
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	vector<int> array = { 1, 5, 2, 6, 3, 7, 4 };
	vector<vector<int>> commands = { {2, 5, 3},{4, 4, 1},{1, 7, 3} };
	vector<int> answer = solution(array, commands);
	for (int a = 0; a < answer.size(); a++) {
		cout << answer[a];
	}
	
}