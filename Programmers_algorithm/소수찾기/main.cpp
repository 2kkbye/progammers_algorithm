#include <string>
#include <vector>

#define SIZE 9999999
#include<iostream>

bool primeNum[SIZE];
bool visit[7];
int maxSize;
int tans = 0;

using namespace std;

void init() {
    for (int a = 0; a < SIZE; a++) {
        primeNum[a] = true;
    }
    for (int a = 0; a < 7; a++) {
        visit[a] = false;
    }
}

void prime() {
    primeNum[0] = false;
    primeNum[1] = false;
    for (int a = 2; a < SIZE; a++) {
        if (!primeNum[a]) continue;
        int index = 2;
        while (a * index < SIZE) {
            primeNum[a * index] = false;
            index++;
        }
    }
}
void dfs(int level, int sum, string numbers) {
    if (maxSize == level) {
        if (primeNum[sum]) {
            tans++;
            primeNum[sum] = false;
        }
        return;
    }
    for (int a = 0; a < maxSize; a++) {
        if (visit[a]) continue;
        visit[a] = true;
        if (primeNum[sum]) {
            tans++;
            primeNum[sum] = false;
        }
        dfs(level + 1, ((sum * 10) + ((int)numbers[a] - 48)), numbers);
        visit[a] = false;
    }
}
int solution(string numbers) {
    init();
    prime();
    maxSize = numbers.length();
    int answer = 0;
    for (int a = 0; a < numbers.length(); a++) {
        int sum = (int)numbers[a] - 48;
        if (primeNum[sum]) {
            answer++;
            primeNum[sum] = false;
        }
        visit[a] = true;
        dfs(1, sum, numbers);
        visit[a] = false;
    }
    answer += tans;
    return answer;
}
int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cout << solution("101");
}