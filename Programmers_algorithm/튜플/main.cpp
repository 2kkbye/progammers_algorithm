#include <string>
#include <vector>
#include<iostream>
#include <stdlib.h>
#define SIZE 500

using namespace std;

int index[SIZE];
void init() {
    for (int a = 0; a < SIZE; a++) {
        index[a] = 0;
    }
}
vector<int> solution(string s) {


    vector<int> answer;
    vector<vector<string>> tuple;
    vector<string> temp;
    int num = 0;

    for (int a = 1; a < s.length() - 1; a++) {
        if (s[a] == '{') continue;
        if (s[a] >= '0' && s[a] <= '9') {
            num = (num * 10) + ((int)s[a] - 48);
            continue;
        }
        if (s[a] == ',') {
            if (num == 0) continue;
            temp.push_back(to_string(num));
            num = 0;
            continue;
        }
        if (s[a] == '}') {
            temp.push_back(to_string(num));
            num = 0;
            index[temp.size()] = tuple.size();
            tuple.push_back(temp);
            temp.clear();
            continue;
        }
    }
    for (int a = 1; a <= tuple.size(); a++) {
        int t = index[a];
        for (int b = 0; b < tuple[t].size(); b++) {

            bool ch = true;
            for (int c = 0; c < answer.size(); c++) {
                if (answer[c] == stoi(tuple[t][b])) {
                    ch = false;
                    break;
                }
            }
            if (ch) {
                answer.push_back(stoi(tuple[t][b]));
            }
        }
    }
    return answer;
}
int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cout << solution("{{2},{2,1},{2,1,3},{2,1,3,4}}").size();
}